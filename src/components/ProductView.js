import { useState, useEffect, useContext, Fragment } from 'react';
import { useParams, useHistory, Link } from 'react-router-dom';
import { Container, Card, Button, Row, Col, InputNumber, InputGroup, FormControl } from 'react-bootstrap';
import UserContext from '../UserContext';
import { add, get, list, remove, quantity, total, destroy } from 'cart-localstorage';
import Swal from 'sweetalert2';

export default function ProductView() {
	const { productId }= useParams()

	const { user } = useContext(UserContext);
	

	const history = useHistory();

	const [id, setId] = useState("");
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(1);

	const [cart, setCart] = useState([]);

	const [isAvailable, setIsAvailable] = useState(true);


		const [isActive, setIsActive] = useState(false);



		const addToCart = () => {

				let alreadyInCart = false;
				let productIndex;
				let message;
				let cart = [];

				if (localStorage.getItem('cart')) {
					cart = JSON.parse(localStorage.getItem('cart'));
				};

				for(let i = 0; i < cart.length; i++){
					if (cart[i].productId === id) {
						alreadyInCart = true;
						productIndex = i;
					}
				}

				if (alreadyInCart) {
					cart[productIndex].quantity += quantity;
					cart[productIndex].subtotal = cart[productIndex].price * cart[productIndex].quantity;
				} else {
					cart.push({
						'productId' : id,
						'name': name,
						'price': price,
						'quantity': quantity,
						'subtotal': price * quantity
					});
				};
		localStorage.setItem('cart', JSON.stringify(cart));
		if (quantity === 1) {
			message = "1 item added to cart.";
		} else {
			message = quantity + ` items added to cart.`;
		}

		alert(message);

	}

	const qtyInput = (value) => {

		if (value === '') {
			value = 1;
		} else if (value === "0") {
			alert("Quantity can't be lower than 1.");
			value = 1;
		}

		setQuantity(value);
	}
	const reduceQty = () => {
			if (quantity <= 1) {
				alert("Quantity can't be lower than 1.");
			} else {
				setQuantity(quantity - 1);
			}
		};


	const addToWishlist = (productId) => {
		fetch('https://tranquil-journey-24938.herokuapp.com/api/users/addToWishlist', {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization:`Bearer ${ localStorage.getItem( 'token' ) }`
				},
			body: JSON.stringify({
				productId: productId,
				name: name
			})
		})
		// .then(res=>res.json())
		.then(res=>res.json())
		.then(data=>{
			console.log(data);

			if(data === true){
				Swal.fire({
						title: "Successfully Added",
						icon: "success",
						text: "You have successfully added the product to your wishlist"
				})
				history.push('/products');
			}else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}
		})
	}

	useEffect(()=>{
		fetch(`https://tranquil-journey-24938.herokuapp.com/api/products/${productId}`)
		.then(res=>res.json())
		.then(data=>{
			console.log(data)

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setQuantity(data.quantity);
			setIsAvailable(data.IsAvailable);
		})
	}, [ productId ]);

const archive = (isAvailable) => {
		fetch(`https://tranquil-journey-24938.herokuapp.com/api/products/${productId}/archive`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization:`Bearer ${ localStorage.getItem( 'token' ) }`
				},
			body: JSON.stringify({
				isAvailable: isAvailable
			})
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data);

			if(data === true){
				Swal.fire({
						title: "Successfully archived",
						icon: "success",
						text: "You have successfully archived the product"
				})
				history.push('/products');
			}else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}
		})
	}
const unarchive = (isAvailable) => {
		fetch(`https://tranquil-journey-24938.herokuapp.com/api/products/${productId}/unarchive`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization:`Bearer ${ localStorage.getItem( 'token' ) }`
				},
			body: JSON.stringify({
				isAvailable: isAvailable
			})
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data);

			if(data === true){
				Swal.fire({
						title: "Successfully archived",
						icon: "success",
						text: "You have successfully archived the product"
				})
				history.push('/products');
			}else{
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}
		})
	}

	useEffect(()=>{

		fetch(`https://tranquil-journey-24938.herokuapp.com/api/products/${productId}`)
		.then(res=>res.json())
		.then(data=>{
			console.log(data)
		})
	}, [ productId ]);

	useEffect(() => {
		if(user.isAdmin == true ){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [name, description, price])
	return(
		<Container className='mt-5 mb-5'>
			<Row>
				<Col lg={ {span: 6, offset: 3} }>
					<Card className="mb-2">
						  <Card.Body className='text-center'>
							    <Card.Title><h2>{name}</h2></Card.Title>
							    <Card.Title>Description:</Card.Title>
							    <Card.Text>{description}</Card.Text>
							    <Card.Title>Reviews:</Card.Title>
							    <Link className="btn btn-info btn-block" to={`/reviews/${productId}`}>See Reviews</Link><br></br>
							    <Card.Title>Price:</Card.Title>
							    <Card.Text>Php {price}</Card.Text>
							    {
							    	user.isAdmin!==true?
									    <Fragment>
									    <Card.Text className="mb-0">Quantity:</Card.Text>
									    <input type="number" className='border border-dark'  id='quantityBox' /> <br /><br />
									    </Fragment>
							    	:
									    <Fragment>
									    <Card.Text hidden>Quantity:</Card.Text>
									    <InputGroup className="quantity mt-2 mb-1">
									    						<InputGroup.Prepend className="d-none d-md-flex">
									    							<Button variant="secondary" onClick={reduceQty}>
									    								-
									    							</Button>
									    						</InputGroup.Prepend>
									    						<FormControl 
									    							type="number"
									    							min="1"
									    							value={quantity}
									    							onChange={e => qtyInput(e.target.value)}
									    						/>
									    						<InputGroup.Append className="d-none d-md-flex">
									    							<Button
									    								variant="secondary"
									    								onClick={() => setQuantity(quantity + 1)}
									    							>
									    								+
									    							</Button>
									    						</InputGroup.Append>
									    					</InputGroup>
									    </Fragment>
							    }
							    {
							    	user.id !== null && isActive ?
							    	<Fragment>
							    	<h4>You Can't Order</h4>
										<Button  variant="primary" className="btn-block mb-0"  onClick={ () => add(productId) } hidden>Order</Button>
									</Fragment>
									:
									<Fragment>
										<Button  variant="primary" onClick={ () => addToCart(productId)  }>Add to Cart</Button>
										<Button  variant="primary" onClick={ () => addToWishlist(productId)  }>Add to Wishlist</Button>
										</Fragment>
							    } <br /> <br />
							    {
							    	isActive ?
							    	<Fragment>
							    	 <Container>
								    		<Button variant="danger" onClick={() =>archive(productId)}>Archive</Button>
								    		<Button variant="danger" onClick={() =>unarchive(productId)}>Unarchive</Button>
											<Link className="btn btn-info btn-block" to={`/${productId}/update`}>Update</Link>
							    		</Container>
							    	</Fragment>
									:
							    	<Fragment>
							    	<p className="mb-0">Already ordered?</p>
							    	<Link className="btn btn-primary btn-block mt-0" to={`/reviews/${productId}/create`}>Create Review</Link>
							    	</Fragment>
							    }
										<div>
											<br /><h4>Bluetooth over anything! ANYTHING!</h4>
										</div>

						  </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}