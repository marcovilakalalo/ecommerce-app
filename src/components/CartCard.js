import { useState, useEffect, useContext, Fragment } from 'react';
import { useParams, useHistory, Link } from 'react-router-dom';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import UserContext from '../UserContext';
import { add, get, list, remove, quantity, total, destroy } from 'cart-localstorage';
import Swal from 'sweetalert2';


export default function Cart() {
	const { productId }= useParams()

	const { user } = useContext(UserContext);
	

	const history = useHistory();

	
	function renderCart(items) {
				const $cart = document.querySelector(".cart")
				const $total = document.querySelector(".total")

				$cart.innerHTML = items.map((item) => `
						<tr>
							<td>#${item.id}</td>
							<td>${item.name}</td>
							<td>${item.quantity}</td>
							<td style="width: 60px;">	
								<button type="button" class="btn btn-block btn-sm btn-outline-primary"
									onClick="cartLS.quantity(${item.id},1)">+</button>
							</td>
							<td style="width: 60px;">	
								<button type="button" class="btn btn-block btn-sm btn-outline-primary"
									onClick="cartLS.quantity(${item.id},-1)">-</button>
							</td>
							<td class="text-right">$${item.price}</td>
							<td class="text-right"><Button class="btn btn-primary" onClick="cartLS.remove(${item.id})">Delete</Button></td>
						</tr>`).join("")

				$total.innerHTML = "$" + cartLS.total()
			}
			renderCart(cartLS.list())
			cartLS.onChange(renderCart)
		<div class="card mb-4 shadow-sm">
			<div class="card-header">
				<h2>Cart</h2>
			</div>
			<div class="card-body">
				<table class="table">
					<tbody class="cart">
					</tbody>
					<tfoot>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td class="text-right">Total: <strong class="total"></strong></td>
						<td></td>
					</tfoot>
				</table>
			</div>
		</div>


}