// import state and effect hooks from react
import { useContext, Fragment } from 'react';
import PropTypes from 'prop-types';
import {Button, Row, Col, Card, Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext';

export default function WishlistCard({wishlistProp}){
	const { _id, name, productId } = wishlistProp

	return(	
			<Fragment>
			<Container className="cardHighlights">
				<Card>
					  <Card.Body>
						    <Card.Title>{name}</Card.Title>
							<Link className="btn btn-primary" to={`/products/${productId}`}>See Products</Link>
					  </Card.Body>
				</Card>
			</Container>
			}
			</Fragment>
	)
}