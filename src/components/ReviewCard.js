// import state and effect hooks from react
import { useContext,Fragment } from 'react';
import PropTypes from 'prop-types';
import {Button, Row, Col, Card, Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext';

export default function ReviewCard({reviewProp}){
	const { user } = useContext(UserContext);
	const { _id, review } = reviewProp

	return(	
			<Container className="mb-4">
				<Card>
				<Fragment>
					  <Card.Body>
						    <Card.Title>Product Review</Card.Title>
						    <Card.Subtitle >{review}</Card.Subtitle>
							<Link className="btn btn-primary" to={`/products`}>See Products</Link>
					  </Card.Body>
				</Fragment>
				</Card>
			</Container>
	)
}