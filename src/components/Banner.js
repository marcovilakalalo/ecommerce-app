import {Button, Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Banner({data}){
	console.log(data)
	const {title, content} = data;
	return(
		<Row>
			<Col className=" p-5">
				<h1 className='bannerText'>{title}</h1>
				<br />
				<h4 className='bannerText'>{content}</h4>
			</Col>
		</Row>
	)
}
