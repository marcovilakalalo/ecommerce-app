import { useState, useEffect, useContext } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import { Redirect, useHistory, Link, useParams } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function CreateProduct(){

	const {user} = useContext(UserContext);

	const { productId }= useParams()

	const history = useHistory();

	const [name, setName] = useState('');
	const [review, setReview] = useState('');
	const [orderId, setOrderId] = useState('');

	const [isActive, setIsActive] = useState(false);


	function createReview(e) {
		e.preventDefault();
		fetch(`https://tranquil-journey-24938.herokuapp.com/api/reviews/${productId}/create`, {
			method: "POST",
			headers: {
			    'Content-Type': 'application/json',
			    'Accept': 'application/json',
			    Authorization:`Bearer ${ localStorage.getItem( 'token' ) }`
			},
			body: JSON.stringify({
				userId: user.id,
			    name: name,
			    orderId: orderId,
			    review: review
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (data === true) {

			    setName('');
			    setReview('')
			    setOrderId('')

			    Swal.fire({
			        title: 'Review successfully created',
			        icon: 'success',
			        text: 'See Reviews page'
			    });

			    history.push("/reviews/:productId");

			} else {

			    Swal.fire({
			        title: `Failed`,
			        icon: 'error',
			        text: 'Please enter a valid order id. You may order first if you forgot it! :)'   
			    });

			}
		})
	}
	useEffect(() => {
		if(name !== '' && orderId!=='' && review !== '' ){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [name, orderId, review])

	return (
		<Container>
			<h1>Create Product Review</h1>
			<Form className="mt-3" onSubmit={(e) => createReview(e)}>
				<Form.Group controlId="name">
				    <Form.Label>Product Name</Form.Label>
				    <Form.Control 
				        type="text" 
				        placeholder="Enter product name"
				        value={name} 
				        onChange={e => setName(e.target.value)}
				        required
				    />
				</Form.Group>
				
				<Form.Group className="mb-3" controlId="description">
			    <Form.Label>Order Id</Form.Label>
			    <Form.Control 
			    	type="text" 
			    	placeholder="Enter order id" 
			    	value = {orderId}
			    	onChange = { e => setOrderId(e.target.value)}
			    	required 
			    />
			  </Form.Group>

				<Form.Group className="mb-3" controlId="description">
			    <Form.Label>Review</Form.Label>
			    <Form.Control 
			    	type="text" 
			    	placeholder="Enter product review" 
			    	value = {review}
			    	onChange = { e => setReview(e.target.value)}
			    	required 
			    />
			  </Form.Group>

			  { isActive ? 
			  		<Button variant="primary" type="submit" id="submitBtn">
			  		  Submit
			  		</Button>
			  	:
				  	<Button variant="primary" type="submit" id="submitBtn" disabled>
				  	  Submit
				  	</Button>
			  }
			</Form>
		</Container>
	)
}
