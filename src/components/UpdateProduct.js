import { useState, useEffect, useContext } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import { Redirect, useHistory, Link, useParams } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function UpdateProduct(){

	const {user} = useContext(UserContext);

	const { productId }= useParams()

	const history = useHistory();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [isAvailable, setIsAvailable] = useState(true);

	const [isActive, setIsActive] = useState(false);


	function update(e) {
		e.preventDefault();
		fetch(`https://tranquil-journey-24938.herokuapp.com/api/products/${productId}/update`, {
			method: "PUT",
			headers: {
			    'Content-Type': 'application/json',
				Authorization:`Bearer ${ localStorage.getItem( 'token' ) }`
			},
			body: JSON.stringify({
				productId: productId,
			    name: name,
			    description: description,
			    price: price,
			    isAvailable: true
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if (data === true) {

			    setName('');
			    setDescription('');
			    setPrice(0);
			    setIsAvailable(true);

			    Swal.fire({
			        title: 'Product updated successfully',
			        icon: 'success',
			        text: 'See Products page'
			    });

			    history.push("/products");

			} else {

			    Swal.fire({
			        title: 'Something wrong',
			        icon: 'error',
			        text: 'Please try again.'   
			    });

			}
		})
	}
	useEffect(() => {
		if(name !== '' && description !== '' && price !== '' ){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [name, description, price])

	return (
		<Container>
			<h1>Update Product Information</h1>
			<Form className="mt-3" onSubmit={(e) => update(e)}>
				<Form.Group controlId="name">
				    <Form.Label>Product Name</Form.Label>
				    <Form.Control 
				        type="text" 
				        placeholder={"Enter product name"}
				        value={name} 
				        onChange={e => setName(e.target.value)}
				        required
				    />
				</Form.Group>
				
				<Form.Group className="mb-3" controlId="description">
			    <Form.Label>Description</Form.Label>
			    <Form.Control 
			    	type="text" 
			    	placeholder="Enter product description" 
			    	value = {description}
			    	onChange = { e => setDescription(e.target.value)}
			    	required 
			    />
			  </Form.Group>

			  <Form.Group className="mb-3" controlId="price">
			    <Form.Label>Price</Form.Label>
			    <Form.Control 
			    	type="number" 
			    	placeholder={"Enter product price"} 
			    	value={price}
			    	onChange = { e => setPrice(e.target.value)}
			    	required 
			    />
			  </Form.Group>
			  { isActive ? 
			  		<Button variant="primary" type="submit" id="submitBtn">
			  		  Update
			  		</Button>
			  	:
				  	<Button variant="primary" type="submit" id="submitBtn" disabled>
				  	  Update
				  	</Button>
			  }
			</Form>
		</Container>
	)
}
