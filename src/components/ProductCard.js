// import state and effect hooks from react
import { useContext } from 'react';
import PropTypes from 'prop-types';
import {Button, Row, Col, Card, Container} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext';

export default function ProductCard({productProp}){
	const { user } = useContext(UserContext);
	const { _id, name, description, price} = productProp

	return(	
			<Container className="cardHighlights">
				<Card className="pb-3">
					  <Card.Body>
						    <Card.Title>{name}</Card.Title>
						    <Card.Subtitle >Price:</Card.Subtitle>
						    <Card.Subtitle >Php {price}</Card.Subtitle>
						    <br />
						    {
						    	user.id !== null?
							<Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
						    	:
							<Link className="btn btn-danger" to={`/login`}>Login to see the details</Link>
						    }
					  </Card.Body>
				</Card>
			</Container>
	)
}