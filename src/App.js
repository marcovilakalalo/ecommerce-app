import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';

import AppNavbar from './components/AppNavbar';
import Footer from './components/Footer';
import UpdateProduct from './components/UpdateProduct';
import ProductView from './components/ProductView'
import CreateReview from './components/CreateReview'

import Reviews from './pages/Reviews'
import Home from './pages/Home';
import Cart from './pages/Cart';
import CreateProduct from './pages/CreateProduct';
import Register from './pages/Register'
import Login from './pages/Login'
import Wishlist from './pages/Wishlist'
import Logout from './pages/Logout'
import Error from './pages/Error'
import Products from './pages/Products'

import './App.css';
import { UserProvider } from './UserContext';


function App() {
    // state hook for the user state that is defined for a global scope
    // initialize an object with properties from the localStorage
    const [user, setUser] = useState({
      id: null,
      isAdmin: null
    })
    // Function for clearing localStorage on logout
    const unsetUser = () => {

    localStorage.clear();

    setUser({
      id: null,
      isAdmin: null
    });

    };

     useEffect(() => {


        fetch(`${ process.env.REACT_APP_API_URL }/api/users/details`, {
          headers: {
            "Content-Type":"application/json",
            Authorization: `Bearer ${ localStorage.getItem('token') }`
          }
        })
        .then(res => res.json())
        .then(data => {

          // Set the user states values with the user details upon successful login.
          if (typeof data._id !== "undefined") {

            setUser({
              id: data._id,
              isAdmin: data.isAdmin
            });

          } else {

            setUser({
              id: null,
              isAdmin: null
            });

          }

        })

        }, []);


  return (
    <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
        <AppNavbar />
          <Container>
              <Switch>
                  <Route exact path="/" component={Home} />
                  <Route exact path="/products" component={Products} />
                  <Route exact path="/products/:productId" component={ProductView} />
                  <Route exact path="/:productId/update" component={UpdateProduct} />
                  <Route exact path="/reviews/:productId" component={Reviews} />
                  <Route exact path="/reviews/:productId/create" component={CreateReview} />
                  <Route exact path="/register" component={Register} />
                  <Route exact path="/login" component={Login} />
                  <Route exact path="/logout" component={Logout} />
                  <Route exact path="/create" component={CreateProduct} />
                  <Route exact path="/:userId/cart" component={Cart} />
                  <Route exact path="/wishlist" component={Wishlist} />
                  <Route component={Error} />
              </Switch>
          </Container>
        </Router>
        <Footer />
    </UserProvider>
  );
}

export default App;