// Mock Database
const courseData = [
	{
		id:"wdc001",
		name: "PHP-Laravel",
		description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sit amet tincidunt libero. Curabitur ullamcorper risus magna, et tempor ex consequat et.",
		price: 45000,
		onOffer: true
	},
	{
		id:"wdc002",
		name: "Phyton-Django",
		description: "Lorem ipsum keme keme keme 48 years jowa antibiotic borta ang na tanders at ang na ang bongga ang doonek at ang.",
		price: 50000,
		onOffer: true
	},
	{
		id:"wdc003",
		name: "JAVA-Springboot",
		description: "Sed tempor dolor in lacus laoreet, at tincidunt dolor auctor. Suspendisse arcu ligula, placerat sit amet ipsum ut, tempor sollicitudin sem. Pellentesque hendrerit enim eget libero iaculis, nec euismod ex molestie. ",
		price: 55000,
		onOffer: true
	},
	{
		id:"wdc004",
		name: "HTML Introduction",
		description: "Aenean ullamcorper consequat tincidunt. Aenean magna ante, rutrum placerat consequat eu, dictum et orci. Donec elementum enim ut eleifend tempus. Aliquam erat volutpat.",
		price: 60000,
		onOffer: true
	}
];

export default courseData;