import { Fragment, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import ReviewCard from '../components/ReviewCard'

export default function Review() {

	const [reviews, setReviews] = useState([]) 
	const { productId } = useParams()

	useEffect(()=>{ 
		fetch(`https://tranquil-journey-24938.herokuapp.com/api/reviews/${productId}`)
		.then(res=>res.json())
		.then(data=>{
			console.log(data);

			setReviews(
				data.map(review =>{
						return(
							<ReviewCard key={review._id} reviewProp={review} />
						);
					})
			)
		})
	}, [] );

	return(
		<Container>
			<h1>Reviews</h1>
			<Fragment>
				{reviews}
			</Fragment>
		</Container>
	)
}