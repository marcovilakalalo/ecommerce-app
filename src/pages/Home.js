
import { Fragment } from 'react';
import { Container } from 'react-bootstrap';
import Banner from '../components/Banner';
import ProductCarousel from '../components/Carousel';
// import CourseCard from '../components/CourseCard';


/*
export default function Home() {
	return(
		<Fragment>
			<Banner />
			<Highlights />
		</Fragment>
	)
}*/


// ACTIVITY ANSWER KEY
export default function Home(){

	const data = {
	    title: "Because Everything is better with Bluetooth",
	    content: "Your one stop for your bluetooth needs"
	}


	return (
		<Fragment >
			<Container className="homeBanner" >
				<Banner data={data} />
				<ProductCarousel className='m-0' />
			</Container >
		</Fragment>
	)
}