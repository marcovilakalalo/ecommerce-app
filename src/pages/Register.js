import { useState, useEffect, useContext } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import { Redirect, useHistory, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register(){

	const {user} = useContext(UserContext);
	const history = useHistory();

	// State hooks to store the values of the input fields
	const [userName, setUserName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [isActive, setIsActive] = useState(false);


	function registerUser(e) {
		e.preventDefault();
		fetch('https://tranquil-journey-24938.herokuapp.com/api/users/checkEmail', {
		    method: "POST",
		    headers: {
		        'Content-Type': 'application/json'
		    },
		    body: JSON.stringify({
		        email: email
		    })
		})
		.then(res => res.json())
		.then(data => {

		    console.log(data);

		    if(data === true){

		    	Swal.fire({
		    		title: 'Duplicate email found',
		    		icon: 'error',
		    		text: 'Kindly provide another email to complete the registration.'	
		    	});

		    }
		    else{
		    	fetch('https://tranquil-journey-24938.herokuapp.com/api/users/register', {
		    		method: "POST",
		    		headers: {
		    		    'Content-Type': 'application/json'
		    		},
		    		body: JSON.stringify({
		    		    userName: userName,
		    		    email: email,
		    		    password: password1
		    		})

		    	})
		    	.then(res => res.json())
		    	.then(data => {
		    		console.log(data);

		    		if (data === true) {

		    		    // Clear input fields
		    		    setUserName('');
		    		    setEmail('');
		    		    setPassword1('');
		    		    setPassword2('');

		    		    Swal.fire({
		    		        title: 'Registration successful',
		    		        icon: 'success',
		    		        text: 'Welcome to Zuitt!'
		    		    });

		    		    history.push("/login");

		    		} else {

		    		    Swal.fire({
		    		        title: 'Something wrong',
		    		        icon: 'error',
		    		        text: 'Please try again.'   
		    		    });

		    		}
		    	})
		    }


		})

	}

	useEffect(() => {
		if((userName !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [userName, email, password1, password2])

	return (
		(user.id !== null) ?
		    <Redirect to="/products" />
		:
		<Container>
			<h1>Register</h1>
			<Form className="mt-3" onSubmit={(e) => registerUser(e)}>
				<Form.Group controlId="firstName">
				    <Form.Label>Username</Form.Label>
				    <Form.Control 
				        type="text" 
				        placeholder="Enter username"
				        value={userName} 
				        onChange={e => setUserName(e.target.value)}
				        required
				    />
				</Form.Group>
				<Form.Group className="mb-3" controlId="userEmail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control 
			    	type="email" 
			    	placeholder="Enter email" 
			    	value = {email}
			    	onChange = { e => setEmail(e.target.value)}
			    	required 
			    />
			    <Form.Text className="text-muted">
			      We'll never share your email with anyone else.
			    </Form.Text>
			  </Form.Group>

			  <Form.Group className="mb-3" controlId="password1">
			    <Form.Label>Password</Form.Label>
			    <Form.Control 
			    	type="password" 
			    	placeholder="Password" 
			    	value={password1}
			    	onChange = { e => setPassword1(e.target.value)}
			    	required 
			    />
			  </Form.Group>
			  
			  <Form.Group className="mb-3" controlId="password2">
			    <Form.Label>Verify Password</Form.Label>
			    <Form.Control 
			    	type="password" 
			    	placeholder="Verify Password" 
			    	value={password2}
			    	onChange = { e => setPassword2(e.target.value)}
			    	required 
			    />
			  </Form.Group>
			{/* Conditionally render submit button based on isActive state */}
			  { isActive ? 
			  		<Button variant="primary" type="submit" id="submitBtn">
			  		  Submit
			  		</Button>
			  	:
				  	<Button variant="primary" type="submit" id="submitBtn" disabled>
				  	  Submit
				  	</Button>
			  }
			</Form>
			<p>Already have an account?</p>
			<Link className="btn btn-light" to={`/login`}>Login</Link>
		</Container>
	)
}
