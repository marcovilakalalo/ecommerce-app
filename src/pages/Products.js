import { Fragment, useEffect, useState } from 'react';
import { Container } from 'react-bootstrap';
import ProductCard from '../components/ProductCard'

export default function Products() {


	// state that will be used to store the products retrieved from the database
	const [products, setProducts] = useState([]) 
	// retrieve the products from the database upon initial render of the "Products" component
	useEffect(()=>{
		fetch('https://tranquil-journey-24938.herokuapp.com/api/products/all')
		.then(res=>res.json())
		.then(data=>{
			console.log(data);

			//sets the "products" state to map the data retrieved from the fetch request into several 'ProductCard' components 
			setProducts(
				data.map(product =>{
						return(
							<ProductCard key={product._id} productProp={product} />
						);
					})
			)
		})
	}, [] );

	return(
		<Container>
			<h1>Products</h1>
			<Fragment>
				{products}
			</Fragment>
		</Container>
	)
}