import React, { useState, useEffect, useContext } from 'react';
import { Container, Modal, InputGroup, Button, FormControl, Table } from 'react-bootstrap'
import { Link, Redirect } from 'react-router-dom';
import UserContext from '../UserContext';

export default function MyCart(){

	const { user } = useContext(UserContext)

	const [total, setTotal] = useState(0);
	const [cart, setCart] = useState([]);
	const [tableRows, setTableRows] = useState([]);
	const [willRedirect, setWillRedirect] = useState(false);

	useEffect(()=> {

		if (localStorage.getItem('cart')) {
			setCart(JSON.parse(localStorage.getItem('cart')));
		}

	}, [])

	useEffect(()=> {

		const quantityInput = (productId, value) => {

			let myCart = [...cart];

			if (value === '') {
				value = 1;
			} else if (value === "0") {
				alert("Quantity can't be lower than 1.");
				value = 1;
			}

			for(let i = 0; i < cart.length; i++){

				if(myCart[i].productId === productId){
					myCart[i].quantity = parseFloat(value);
					myCart[i].subtotal = myCart[i].price * myCart[i].quantity;
				}

			}

			setCart(myCart);
			localStorage.setItem('cart', JSON.stringify(myCart));

		}

		const button = (productId, operator) => {

			let myCart = [...cart];

			for(let i = 0; i < myCart.length; i++){

				if (myCart[i].productId === productId) {

					if (operator === "+") {
						myCart[i].quantity += 1;
						myCart[i].subtotal = myCart[i].price * myCart[i].quantity;
					} else if (operator === "-") {
						if(myCart[i].quantity <= 1){
							alert("Quantity can't be lower than 1.");
						}else{
							myCart[i].quantity -= 1;
							myCart[i].subtotal = myCart[i].price * myCart[i].quantity;
						}
					}

				}
			}

			setCart(myCart);
			localStorage.setItem('cart', JSON.stringify(myCart));

		}

		const removeBtn = (productId) => {

			let myCart = [...cart];

			let cartIds = cart.map((item)=> {
				return item.productId;
			})

			myCart.splice([cartIds.indexOf(productId)], 1);

			setCart(myCart);
			localStorage.setItem('cart', JSON.stringify(myCart));

		}

		let cartItems = cart.map((item, index) => {

			return (
		      <tr key={item.productId}>
		          <td>
		          	<Link to={`/products/${item.productId}`}>
		          		{item.name}
		          	</Link>
		          </td>
		          <td>₱{item.price}</td>
		          <td>
				  	<InputGroup className="d-md-none">
						<FormControl
							type="number"
							min="1"
							value={item.quantity}
							onChange={e => quantityInput(item.productId, e.target.value)}
						/>
					</InputGroup>
					<InputGroup className="d-none d-md-flex w-50">

						<InputGroup.Prepend>
							<button 
								variant="secondary"
								onClick={() => button(item.productId, "-")}
							>
								-
							</button>
						</InputGroup.Prepend>

						<FormControl
							type="number"
							min="1" value={item.quantity}
							onChange={e => quantityInput(item.productId, e.target.value)}
						/>

						<InputGroup.Append>
							<button 
								variant="secondary"
								onClick={() => button(item.productId, "+")}
							>
								+
							</button>
						</InputGroup.Append>

					</InputGroup>
		          </td>
		          <td>₱{item.subtotal}</td>
		          <td className="text-center">
		          	<button 
		          		variant="danger"
		          		onClick={() => removeBtn(item.productId)}
		          	>
		          		Remove
		          	</button>
		          </td>
		      </tr>			
			);

		})

		setTableRows(cartItems);

		let totalAmmount = 0;

		cart.forEach((item)=> {
			totalAmmount += item.subtotal;
		});

		setTotal(totalAmmount);

	}, [cart]);

	const checkout = () => {

		const checkoutCart =  cart.map((item) => {
			return {
				productName: item.name,
				quantity: item.quantity,
				price: item.price,
			}
		})

		fetch(`${ process.env.REACT_APP_API_URL}/api/users/checkout`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				products: checkoutCart,
				totalAmount: total
			})
		})
		.then(res => res.json())
		.then(data => {

			if (data === true) {
				alert("Order placed! Thank you!");
				localStorage.removeItem('cart');
				setWillRedirect(true);
			} else {
				alert("Something went wrong. Order was NOT placed.");
			}

		})
	}

	return(
		willRedirect === true ? 
			<Redirect to="/orders"/>
		:
			cart.length <= 0 ? 
					<Modal>
						<h3 className="text-center">
							Your cart is empty! <Link to="/products">Start shopping.</Link>
						</h3>
					</Modal>
				:
				<Container>
					<h2 className="text-center my-4">Your Shopping Cart</h2>
					<Table striped bordered hover responsive>
						<thead className="bg-secondary text-white">
							<tr>
								<th>Name</th>
								<th>Price</th>
								<th>Quantity</th>
								<th>Subtotal</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							{tableRows}
							<tr>
								<td colSpan="3">
									<button 
										variant="success"
										block
										onClick={()=> checkout()}
									>
										Checkout
									</button>
								</td>
								<td colSpan="2">
									<h3>Total: ₱{total}</h3>
								</td>
							</tr>
						</tbody>						
					</Table>
				</Container>
	);
	
}
